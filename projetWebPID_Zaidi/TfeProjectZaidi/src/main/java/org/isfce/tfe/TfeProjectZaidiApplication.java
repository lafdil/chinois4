package org.isfce.tfe;

import org.isfce.tfe.dao.IEtudiantJpaDao;
import org.isfce.tfe.model.Etudiant;
import org.isfce.tfe.model.Roles;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.RequiredArgsConstructor;

@SpringBootApplication
// create a constructor for the attribute
@RequiredArgsConstructor

public class TfeProjectZaidiApplication implements CommandLineRunner {

	private IEtudiantJpaDao etudiantJpaDao;

	public static void main(String[] args) {
		SpringApplication.run(TfeProjectZaidiApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {

		/*
		 * Etudiant etud1 = new Etudiant("etu1", Roles.ROLE_ETUDIANT,
		 * "$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq", "Didier",
		 * "john.doe@example.com", "555-1234", "etudVo");
		 */
		//etudiantJpaDao.save(etud1);
	}

}
