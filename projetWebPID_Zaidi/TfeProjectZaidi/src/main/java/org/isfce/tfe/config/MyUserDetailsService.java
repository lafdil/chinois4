package org.isfce.tfe.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.isfce.tfe.dao.IEtudiantJpaDao;
import org.isfce.tfe.dao.IUserJpaDao;
import org.isfce.tfe.model.Etudiant;
import org.isfce.tfe.model.SecurityUser;
import org.isfce.tfe.model.User;

/**
 * Retourne la liste des utilisateurs pour obtenir les "UserDetails" de
 * l'utilisateur connecté Nécessaire pour la configuration de la sécurité
 * 
 * @author Didier
 *
 */

@Service(value = "myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	private IEtudiantJpaDao etudiantJpaDao;

	// injection using constructor
	public MyUserDetailsService(IEtudiantJpaDao etudiantJpaDao) {
		this.etudiantJpaDao = etudiantJpaDao;
	}
//MyUserDetailsService return a new SecurityUser
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> oEtudiant = etudiantJpaDao.findById(username);
		List<GrantedAuthority> authorities = new ArrayList<>();
		if (oEtudiant.isPresent()){
			authorities.add(new SimpleGrantedAuthority(oEtudiant.get().getRole().name()));
			return new org.springframework.security.core.userdetails.User(oEtudiant.get().getUsername(), oEtudiant.get().getPassword(), authorities);
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

}
