package org.isfce.tfe.config;

import java.time.LocalDate;
import java.util.ArrayList;

import org.isfce.tfe.controller.dto.SandwichDto;
import org.isfce.tfe.model.SandwichDtoWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
@EnableWebSecurity
public class SecurityConfig {

	// Encodeur pour les passwords lors du login
	//Here, the BCryptPasswordEncoder class is used to encode passwords using the bcrypt algorithm.
	//Spring Security provides several implementations of the PasswordEncoder interface, such as BCryptPasswordEncoder,
	@Bean
	PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	
	
	  @Bean 
	  public SandwichDtoWrapper sandwichDtoWrapper() { 
	  return new  SandwichDtoWrapper(new ArrayList<SandwichDto>()); }
	 
	
	// this bean allows us to access h2 database and browse it 
	@Bean
	@Profile("dev")
	WebSecurityCustomizer webSecurityCustomizer() {
		return (web) -> web.ignoring().antMatchers("/h2/**");
		// .antMatchers("/images/**", "/webjars/**", "/js/**","/css/**");
	}

	
	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
		return http.build();
	}

}
