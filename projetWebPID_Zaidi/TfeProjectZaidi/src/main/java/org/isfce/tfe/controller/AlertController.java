package org.isfce.tfe.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.isfce.tfe.model.Alert;
import org.isfce.tfe.model.Ingredient;
import org.isfce.tfe.service.AlertServices;
import org.isfce.tfe.service.IngredientServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/alert")
public class AlertController {
	@Autowired
	private IngredientServices ingredientServices;

	@Autowired
	private AlertServices alertServices;

	@GetMapping("/checkThreshold")
	public String checkThreshold(Model model) {

		List<Ingredient> ingredients = ingredientServices.findAll();
		List<Alert> alerts = new ArrayList<>();
		for (Ingredient ingredient : ingredients) {
			if (ingredient.getQuantiteStock() <= ingredient.getSeuil()) {

				Alert alert = new Alert(ingredient, LocalDate.now());
				alertServices.save(alert);
				alerts.add(alert);
			}
		}
		// add the list of ingredients to the model

		model.addAttribute("ingredients", ingredients);
		model.addAttribute("alertsList", alerts);
		int nbAlertsOfQuantities = alerts.size();
		model.addAttribute("nbAlertsOfQuantities", nbAlertsOfQuantities);

		return "alerte/alertsListOfQuantities";
	}

	// I have to add an alertType, notified: A boolean, recipient see concept's file

}
