package org.isfce.tfe.controller;

import java.util.List;

import org.isfce.tfe.model.Sandwich;
import org.isfce.tfe.service.CommandeServices;
import org.isfce.tfe.service.IngredientServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;



@Controller
public class CommandeController {
	
	private CommandeServices commandeServices;
	
	// from here I call my homepage!!  that is the root
	 @GetMapping("/")
	    public String home(Model model) {
	        model.addAttribute("welcome", "WELCOME TO YOUR SANDWICH SHOP AT ISFCE ETTERBEEK!");
	        return "home";
	    }
	
}
