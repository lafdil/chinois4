package org.isfce.tfe.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.isfce.tfe.controller.dto.IngredientDto;
import org.isfce.tfe.controller.exceptions.NotExistException;
import org.isfce.tfe.service.IngredientServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/ingredient")
public class IngredientController {

	private IngredientServices ingredientServices;

	@Autowired
	public IngredientController(IngredientServices ingredientServices) {
		this.ingredientServices = ingredientServices;
	}

	/**
	 * Liste des Ingredients
	 * 
	 * @param model
	 * @return la nom logique de la vue qui affichera la liste des Ingredients
	 */
	@GetMapping("/liste")
	public String listeIngredients(Model model) {
		model.addAttribute("ingredientsList", ingredientServices.findAll());

		log.info("liste des Ingredients");
		return "ingredient/listeIngredients";
	}

	@GetMapping("/add")
	public String addIngredientGet(@ModelAttribute("ingredient") IngredientDto ingredient) {
		log.debug("affiche la vue pour ajouter un ingredient ");

		ingredient.setId(ingredientServices.getNumberOfIngredients() + 1);
		// model.addAttribute("savedId", ingredient.getId());
		return "ingredient/addIngredient";
	}

	@PostMapping("/add")
	public String addIngredientPost(@Valid @ModelAttribute("ingredient") IngredientDto ingredientDto,
			BindingResult errors, RedirectAttributes rModel) {
		log.info("POST d'un Ingredient" + ingredientDto);
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			log.debug("Erreurs dans les données du cours:" + ingredientDto.getNomProduit());
			return "ingredient/addIngredient";
		}

		// Vérifie que ce cours n'existe pas encore
		// Register a field error for the specified field of the current object
		// void rejectValue(String field, String errorCode, String defaultMessage)
		if (ingredientServices.exists(ingredientDto.getId())) {
			errors.rejectValue("id", "ingredient.id.doublon", "DUPLICATE ERROR");
			return "ingredient/addIngredient";
		}
//lafdil vid 4eme du 25oct 26 min que j'insert dans ma base de donnes via ma couche de service
		ingredientServices.insert(ingredientDto.toIngredient());

		/*
		 * vid 8nov--19min Préparation des attributs Flash pour survivre à la
		 * redirection Ainsi dans l'affichage du détail du cours on ne devra pas
		 * chercher le cours dans la BD
		 */
		rModel.addFlashAttribute("ingredient", ingredientDto);

		log.debug("redirection ingredient/liste:");

		// lafdil vid 4eme du 25oct 26 min je renvoie pas un nom logique de vue mais un
		// string qui correspond a un URL
		return "redirect:/ingredient/liste";
	}

	/**
	 * Méthode Get pour afficher la page d'édition d'un ingredient
	 * 
	 * @param code  :l'id de l'ingredient
	 * @param model auquel sera ajouté le savedID et l'ingredient à modifier
	 * @return la vue logique video
	 */
	@GetMapping("/{ingredientId}/update")
	public String updateIngredientGet(@PathVariable(name = "ingredientId") int id, Model model) {
		// rajoute l'ingredientDTO ou déclenche une exception
		model.addAttribute("ingredient", (ingredientServices.findOne(id).toDto()));
		// rajoute l'id du cours pour gérer la maj de ce dernier (problème de maj de la
		// PK)
		log.debug("affiche la vue pour modifier un ingredient ");
		return "ingredient/updateIngredient";
	}

	/**
	 * 
	 * @param id            désigne l'id de l'ingredient avant modification
	 * @param ingredientDto l'objet ingredient modifié
	 * @param errors        les erreurs de validation
	 * @param model         pour rajouter des attributs
	 * @param rModel        le modèle pour la redirection
	 * @return video
	 */
	// The @PathVariable annotation is used to bind the idIngredient parameter
	// to the value of the {ingredient} path variable in the request.
	@PostMapping("{ingredient}/update")
	public String updateIngredientPost(@PathVariable(name = "ingredient") int idIngredient,
			@ModelAttribute("ingredient") @Valid IngredientDto ingredientDto, BindingResult errors, Model model,
			RedirectAttributes rModel) {
		log.info("POST update d'un ingredient id: " + idIngredient + " en " + ingredientDto);

		// means that the ingredient with this ID does not exist,
		if (idIngredient == 0)
			throw new NotExistException(0);
		// Gestion de la validation en fonction des annotations
		if (errors.hasErrors()) {
			// rajoute id ingredient pour savoir le code initial de l'ingredient
			model.addAttribute("savedId", idIngredient);
			log.info("Erreurs dans les données de l'ingredient:" + ingredientDto.getId());
			return "ingredient/updateIngredient";
		}
		/*
		 * If there are no validation errors and the id field of the IngredientDto
		 * object is equal to the idIngredient parameter,
		 */
		if (ingredientDto.getId() == idIngredient)

			ingredientServices.update(ingredientDto.toIngredient());
		else
		/*
		 * the exists() method of the ingredientServices object is called with the id
		 * field of the IngredientDto object. If the method returns true, a validation
		 * error message is added to the errors object
		 */
		if (ingredientServices.exists(ingredientDto.getId())) {
			// rajoute idIngredient pour savoir l'id initial de l'ingredient
			model.addAttribute("savedId", idIngredient);
			errors.rejectValue("id", "ingredient.id.doublon", "DUPLICATE ERROR");
			return "ingredient./updateIngredient.";
		} else { // supprime l'objet avec l'ancien code et rajoute le nouveau
			ingredientServices.delete(idIngredient);
			ingredientServices.insert(ingredientDto.toIngredient());
		}

		// Préparation des attributs Flash pour survivre à  la redirection
		// Ainsi dans l'affichage du détail de l'ingredient on ne devra pas chercher
		// l'ingredient dans la BD
		rModel.addFlashAttribute("ingredient", ingredientDto);

		log.debug("redirection détail de l'ingredient");
		return "redirect:/ingredient/"+ingredientDto.getId();
	}

	/**
	 * Affiche le détail d'un ingredient
	 * 
	 * @param id
	 * @param model
	 * @param locale lafdil vid 4eme du 25oct 27min dans (Model model) dans model il
	 *               y aura deja ingredientDTO qu' on vient de creer
	 * @return
	 */
	@GetMapping("/{id}")
	public String detailIngredient(@PathVariable int id, Model model) {
		log.debug("Recherche d'un ingredient: " + id);
		// si ce n'est pas une redirection, charge l' ingredient
		// ces 3 lignes de codes ne seront pas executer car mon model contient deja le
		// cours
		if (!model.containsAttribute("ingredient"))
			// recherche le cours dans la liste et ajout au model
			model.addAttribute("ingredient", (ingredientServices.findOne(id).toDto()));
		return "ingredient/ingredient";
	}

	/**
	 * Supression d'un ingredient
	 * 
	 * @param id de l'ingredient
	 * @return le mapping de redirection
	 */
	@PostMapping("/{id}/delete")
	public String deleteIngredient(@PathVariable int id) {
		if (ingredientServices.exists(id))
			ingredientServices.delete(id);
		else
			throw new NotExistException(id);
		log.debug("Suppression réussie de l'ingredient: " + id);
		return "redirect:/ingredient/liste";
	}

}
