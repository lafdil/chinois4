package org.isfce.tfe.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.isfce.tfe.controller.dto.SandwichDto;
import org.isfce.tfe.controller.exceptions.NotExistException;
import org.isfce.tfe.dao.IUserJpaDao;
import org.isfce.tfe.model.Ingredient;
import org.isfce.tfe.model.Sandwich;
import org.isfce.tfe.model.SandwichDtoWrapper;
import org.isfce.tfe.model.User;
import org.isfce.tfe.service.IngredientServices;
import org.isfce.tfe.service.SandwichServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/sandwich")
public class SandwichController {

	@Autowired
	private SandwichServices sandwichServices;
	@Autowired
	private IngredientServices ingredientServices;
	// @Autowired
	//private IUserJpaDao userJpaDao;
//the @PostM.. is used to map the method to the /sandwich/order URL, 
	// @ModelAtt is used to automatically bind the form data to a SandwichDtoWrapper
	// object.

	@PostMapping("/order")
	public String placeOrder(@ModelAttribute("sandwichDtoWrapper") SandwichDtoWrapper sandwichDtoWrapper, Model model) {

		// recuperer tous les sandwich qui ont ete selectionne pas l'utilisateur
		// selectedSandwiches il n' ya qu'un seul dto

		// Process the sandwich data submitted by the form
		// selectedSandwiches are the real selected sandw , if the user does not select
		// any sand ...the list will be empty
		List<SandwichDto> selectedSandwiches = sandwichDtoWrapper.getSandwichesDto().stream()
				.filter((s) -> s.isSelected()).collect(Collectors.toList());

		// selectedSandwiches contains only sandwiches coming from the database and
		// correspnds to their Dtos
		// when we can get an exception
		List<Sandwich> selectedSandwiches2 = sandwichDtoWrapper.getSandwichesDto().stream()
				.filter((s) -> s.isSelected()).map((sDto) -> sandwichServices.findOne(sDto.getId()))
				.collect(Collectors.toList());

		if (selectedSandwiches == null)
			System.out.println("brolbrol");

		else {
			System.out.println("Selected sandwiches:");
			for (SandwichDto sandwichDto : selectedSandwiches) {
				System.out.println(sandwichDto.getName() + " - " + sandwichDto.getPrice());
				Set<Ingredient> ingredients = sandwichDto.toSandwich().getIngredients();
				System.out.println("Ingredients:");
				for (Ingredient ingredient : ingredients) {
					System.out.println(ingredient);
				}
			}
		}

		for (Sandwich selectedSandwich : selectedSandwiches2) {
			Set<Ingredient> ingredients = selectedSandwich.getIngredients();
			for (Ingredient ingredient : ingredients) {
				// int usedQuantity = ingredient.getMinimunQtity() * selectedSandwiches.size();

				/*
				 * int Qtity = selectedSandwich.toDto().getQuantity(); int usedQuantity =
				 * ingredient.getMinimunQtity() * Qtity;
				 */
				
				int usedQuantity = ingredient.getMinimunQtity();
				selectedSandwich.toDto().getQuantity();


				ingredient.decrementIngredientQuantity(usedQuantity);
				ingredientServices.save(ingredient);
			}
		}

		/*
		 * for (SandwichDto selectedSandwich : selectedSandwiches) { Sandwich sandwich =
		 * selectedSandwich.toSandwich(); Set<Ingredient> ingredients =
		 * sandwich.getIngredients(); for (Ingredient ingredient : ingredients) { int
		 * usedQuantity = ingredient.getMinimunQtity() * selectedSandwich.getQuantity();
		 * ingredient.decrementIngredientQuantity(usedQuantity);
		 * ingredientServices.save(ingredient); } }
		 */

		// it must be SandwichDto
		// double TotalPrice = selectedSandwiches.stream().mapToDouble((s)
		// ->s.getPrice()).sum();
		double TotalPrice = selectedSandwiches.stream().mapToDouble(SandwichDto::calculatetotalPrice).sum();
		System.out.println(TotalPrice);

		/*
		 * User user = userRepository.findByUsername(principal.getName());
		 * user.setCreditBalance(user.getCreditBalance() - totalPrice);
		 * userRepository.save(user);
		 */
		
		model.addAttribute("totalPrice", TotalPrice);
		model.addAttribute("selectedSandwiches", selectedSandwiches);

		// one user--makes orders---may be contains 3 sandwiches--stram

		// double totalPrice =
		// selectedSandwiches.stream().mapToDouble(Sandwich::getPrice).sum();

		return "sandwich/orderSummary";
	}
	/*
	 * The method creates a new SandwichOrder object with an empty list of
	 * sandwiches the current date as the order date, and an empty string as the
	 * customer name. This allows the SandwichController to inject a default
	 * SandwichOrder object as a dependency using the @Autowired annotation, which
	 * can then be used to store and manage sandwich orders.
	 */

	/**
	 * Liste des Sandwiches
	 * 
	 * @param model
	 * @return la nom logique de la vue qui affichera la liste des Sandwiches
	 */
	@GetMapping("/liste")
	public String listeSandwiches(Model model) {
		// because of NoArgsConstructor
		SandwichDtoWrapper sandwichDtoWrapper = new SandwichDtoWrapper();
		// retrieves all sandwiches from the database
		List<Sandwich> sandwiches = sandwichServices.findAll();
		// empty list of SandwichDto objects
		// loops through each Sandwich in the sandwiches list, converts them to a list
		// of SandwichDto objects
		List<SandwichDto> sandwichDtos = new ArrayList<>();
		for (Sandwich sandwich : sandwiches) {
			sandwichDtos.add(sandwich.toDto());
		}
		// The list of SandwichDto objects is then added to a SandwichDtoWrapper object,
		// which is used to pass the data to the view.
		sandwichDtoWrapper.setSandwichesDto(sandwichDtos);
		sandwichDtoWrapper.getSandwichesDto().stream().forEach((s) -> System.out.println(s.getIngredients()));
		model.addAttribute("sandwichDtoWrapper", sandwichDtoWrapper);

		log.info("list of sandwichesDto has been retrieved");
		return "sandwich/listeSandwiches";
	}

	/**
	 * Affiche le détail d'un Sandwich
	 * 
	 * @param id
	 * @param model
	 * @param locale lafdil vid 4eme du 25oct 27min dans (Model model) dans model il
	 *               y aura deja sandwichtDTO qu' on vient de creer
	 * @return
	 */
	@GetMapping("/{id}")
	public String detailSandwich(@PathVariable int id, Model model) {
		log.debug("Recherche d'un sandwich: " + id);
		// si ce n'est pas une redirection, charge le sandwich
		// ces 3 lignes de codes ne seront pas executer car mon model contient deja le
		// sandwich
		if (!model.containsAttribute("sandwich")) {

			Optional<Sandwich> OptionalSandwich = Optional.ofNullable(sandwichServices.findOne(id));

			Sandwich sandwich = OptionalSandwich.get();
			SandwichDto sandwichDto = sandwich.toDto();
			model.addAttribute("sandwich", sandwichDto);
		}
		return "sandwich/sandwich";
	}

	@GetMapping("/add")
	public String addSandwichGet(@ModelAttribute("sandwich") SandwichDto sandwich) {
		log.debug("affiche la vue pour ajouter un sandwich ");
		sandwich.setId(sandwichServices.getNumberOfSandwiches() + 1);
		// ingredient.setId(ingredientServices.getNumberOfIngredients()+ 1);
		// model.addAttribute("savedId", ingredient.getId());
		Set<Ingredient> ingredients = sandwich.getIngredients();
		for (Ingredient ingredient : ingredients) {
			System.out.println(ingredient.getNomProduit());
		}

		return "sandwich/addSandwich";
	}

	@GetMapping("/{sandwich}/update")
	public String updateModuleGet(@PathVariable(name = "sandwich") int sandwich, Model model) {

// getSandwichById is called to retrieve the Sandwich object with the specified
		// ID
		Optional<Sandwich> sandwichOptional = sandwichServices.getSandwichById(sandwich);
//case where the sandwich with the specified ID does  exist.
		if (sandwichOptional.isPresent()) {
			Sandwich sandwichOp = sandwichOptional.get();
//convert Sandwich object to a SandwichDto
			SandwichDto sandwichDto = sandwichOp.toDto();
//The SandwichDto object is added as an attribute to the model with the name "sandwich".
			model.addAttribute("sandwich", sandwichDto);
			log.debug("affiche la vue pour modifier le Sandwich ");
			return "sandwich/updateSandwich";
		} else {
			log.debug("Sandwich not found for id: {}", sandwich);
			return "sandwich/sandwichNotFound";
		}
	}

	@PostMapping("/add")
	public String postCertificatForm(@ModelAttribute("sandwichDto") @Valid SandwichDto sandwichDto,
			BindingResult errors, Model model) {

		if (errors.hasErrors()) {
			log.debug("Erreurs dans les données du sandwichDto:" + sandwichDto.getId());
			return ("sandwich/addsandwichDto");
		}
		System.out.println("Hi lafdil");

		sandwichServices.insert(sandwichDto.toSandwich());
		System.out.println("the operation of insertion has been inserted done successfully");
		return "redirect:/sandwich/liste";
	}

	@GetMapping("/{id}/ingredients")
	public String getSandwichIngredients(@PathVariable("id") int sandwichId, Model model) {

		// uses the sandwichId parameter to find the sandwich in the database
		Optional<Sandwich> OptionalSandwich = Optional.ofNullable(sandwichServices.findOne(sandwichId));

		Sandwich sandwich = OptionalSandwich.get();

		// creates a new ArrayList of Ingredient objects from the Set of Ingredient
		// objects in the Sandwich object
		List<Ingredient> ingredients = new ArrayList<>(sandwich.getIngredients());
		model.addAttribute("sandwich", sandwich);
		model.addAttribute("ingredients", ingredients);
		System.out.println("Hi zaidi");
		return "sandwich/sandwich_ingredients";
	}
}