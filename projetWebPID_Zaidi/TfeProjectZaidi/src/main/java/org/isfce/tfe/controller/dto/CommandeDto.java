package org.isfce.tfe.controller.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.isfce.tfe.model.LigneCmd;
import org.isfce.tfe.model.User;
import org.springframework.format.annotation.DateTimeFormat;

public class CommandeDto {


	private int id;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime orderDateTime;

	@ManyToOne
	@JoinColumn(name = "FKUSER")
	private User user;

	@OneToMany(fetch = FetchType.LAZY)
	Set<LigneCmd> LigneCommandes = new HashSet<>();

	
}
