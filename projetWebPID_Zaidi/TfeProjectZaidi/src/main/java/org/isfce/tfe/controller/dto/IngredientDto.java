package org.isfce.tfe.controller.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


import javax.validation.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.isfce.tfe.model.Ingredient;
import org.isfce.tfe.model.Sandwich;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IngredientDto {

	private int id;

	// @Size le nom du cours should have at least 4 characters
	@NotBlank
	@Size(min = 4, max = 60, message = "{elem.nom}")
	private String nomProduit;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateExpiration;

	@NotNull
	private int quantiteStock;

	@NotNull
	private int seuil;
	
	@NotNull
	private int minimunQtity;
	
	@NotNull
	private double priceOfMinimunQtity;

	public IngredientDto(int id, String nomProduit, LocalDate dateExpiration, int quantiteStock, int seuil, int minimunQtity,
			double priceOfMinimunQtity) {
		this.id = id;
		this.nomProduit = nomProduit;
		this.dateExpiration = dateExpiration;
		this.quantiteStock = quantiteStock;
		this.seuil = seuil;
		this.minimunQtity= minimunQtity;
		this.priceOfMinimunQtity = priceOfMinimunQtity;
	}

	public Ingredient toIngredient() {
		Ingredient ingredient = new Ingredient(this.id, this.nomProduit, this.dateExpiration, this.quantiteStock,
				this.seuil, this.minimunQtity=minimunQtity ,this.priceOfMinimunQtity);
		return ingredient;
	}
	
	 

}