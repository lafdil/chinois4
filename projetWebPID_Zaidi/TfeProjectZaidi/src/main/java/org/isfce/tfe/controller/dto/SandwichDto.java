package org.isfce.tfe.controller.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.isfce.tfe.model.Ingredient;
import org.isfce.tfe.model.Sandwich;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SandwichDto {

	private int id;
	@NotNull
	private String name;

	@DecimalMin("0.0")
	private double price;
	protected Set<Ingredient> ingredients = new HashSet<Ingredient>();
	// Just for the binding, are not used in the constructor because we already know
	// their values
	private boolean selected;
	public int quantity;

	public SandwichDto(int id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.selected = false;
		this.quantity = 0; // by default the quantity is 0 but the user can order later
	}

	/*
	 * public boolean getIsSelected() { return selected; } public int getQuantity()
	 * { return quantity; }
	 */

	public Sandwich toSandwich() {

		Sandwich sandwich = new Sandwich(this.id, this.name, this.price);
		sandwich.setIngredients(this.ingredients);
		return sandwich;
	}

	public double calculatetotalPrice() {

		return this.quantity * this.price;
	}
	
	 

}
