package org.isfce.tfe.controller.exceptions;

//Map cette exception sur une erreur HTTP 404 
//@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Elément non trouvé!")
public class NotExistException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	// identifiant de l'objet recherche
	private int id;

	/**
	 * Exception qui indique que l'objet ayant le id indiqué n'existe pas message
	 * 
	 * @param message
	 * @param id
	 */
	public NotExistException(String message, int id) {
		super(message);
		this.id = id;
	}
	/**
	 * Exception qui indique que l'objet ayant le id indiqué n'existe pas message
	 * 
	 * @param message
	 * @param id
	 */
	public NotExistException(int id) {
		this("error.object.notExist",id);
	}
	
	public int getId() {
		return id;
}
}
