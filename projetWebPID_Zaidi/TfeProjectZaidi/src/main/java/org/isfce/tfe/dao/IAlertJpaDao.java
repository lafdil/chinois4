package org.isfce.tfe.dao;

import org.isfce.tfe.model.Alert;
import org.isfce.tfe.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IAlertJpaDao extends JpaRepository<Alert,Integer>{

}
