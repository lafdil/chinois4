package org.isfce.tfe.dao;

import org.isfce.tfe.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICommandeJpaDao extends JpaRepository<Ingredient,Integer>  {

}
