package org.isfce.tfe.dao;

import org.isfce.tfe.model.Ingredient;
import org.isfce.tfe.model.Sandwich;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ISandwichJpaDao extends JpaRepository<Sandwich,Integer>{

}
