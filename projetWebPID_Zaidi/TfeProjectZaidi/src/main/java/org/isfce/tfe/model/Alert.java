package org.isfce.tfe.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor

@Entity(name = "TAlert")
public class Alert {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FKPRODUIT")
	private Ingredient ingredient;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate AlertDate;


	@Column(name = "INGREDIENT_NAME")
	private String ingredientName;


	@Column(name = "INGREDIENT_QUANTITY")
	private int ingredientQuantity;

	public Alert(Ingredient ingredient, LocalDate alertDate) {
		super();
		this.ingredient = ingredient;
		AlertDate = alertDate;
	}

}
