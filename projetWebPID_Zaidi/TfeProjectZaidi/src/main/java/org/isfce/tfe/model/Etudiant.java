
package org.isfce.tfe.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data

@Entity(name = "TETUDIANT")

// we do not have to create a colomun for this ..it will be generated automatically
@DiscriminatorValue("E")
public class Etudiant extends User{


	@Size(min = 1, max = 40, message = "{elem.nom}")
	@Column(length = 40)
	private String numEtudiant ;

	public Etudiant(String numEtudiant) {
		super();
		this.numEtudiant = numEtudiant;
	}

	/*
	 * public Etudiant(String username, Roles role, String password, String prenom,
	 * String email, String tel, String numEtudiant, double creditBalance) {
	 * super(username, role, password, prenom, email, tel, creditBalance);
	 * this.numEtudiant = numEtudiant; }
	 */


	
}
