package org.isfce.tfe.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.isfce.tfe.controller.dto.IngredientDto;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor

@Entity(name = "TINGREDIENT")
public class Ingredient {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@NotNull
	@Column(name = "NAME_INGREDIENT", length = 40, nullable = false)
	private String nomProduit;

	@NotNull
	@Column(name = "EXPIRATION_DATE", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateExpiration;

	@NotNull
	@Column(name = "STOCK_QTY")
	private int quantiteStock;

	@NotNull
	@Column(name = "THRESHOLD")
	private int seuil;

	@NotNull
	@Column(name = "MIN_QTY")
	private int minimunQtity;

	@NotNull
	@Column(name = "PRICE_OF_MINI_QTITY", nullable = false)
	private double priceOfMinimunQtity;

	// we can create a dto from currentIngredient
	public IngredientDto toDto() {
		return new IngredientDto(this.id, this.nomProduit, this.dateExpiration, this.quantiteStock, this.seuil,
				this.minimunQtity, this.priceOfMinimunQtity);

	}

	public void decrementIngredientQuantity(int quantity) {
		this.quantiteStock -= quantity;
	}
	
	/*
	 * @oneToMany(mappedBy = 'ingredient') private list<alerte> alertes;
	 */

	
	
	/*
	 * // price in Euro
	 * 
	 * @NotNull
	 * 
	 * @Column(name = "PRICE") private double prix;
	 */

	/*
	 * @ManyToOne(fetch = FetchType.EAGER)
	 * 
	 * @JoinColumn(name = "FKCATEGORIE") private Categorie Categorie;
	 */

}
