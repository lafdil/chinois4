package org.isfce.tfe.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor

public class MenuSandwich {
    private String name;
    private Sandwich sandwich;
    
    // constructors, getters, and setters
    
/*    public MenuSandwich(String name, List<Sandwich> sandwiches) {
        this.name = name;
        this.sandwiches = sandwiches;*/
    }
    
    
