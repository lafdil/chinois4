package org.isfce.tfe.model;

import java.time.LocalDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class Payment {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    @JoinColumn(name = "FK_COMMANDE")
    private Commande commande;

    private double amountPaid;

    private LocalDateTime paymentDateTime;


}
