package org.isfce.tfe.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name = "TPROFESSEUR")
//Rajout d'une contrainte d'unicité sur la table


@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "nom", "prenom" }))
@DiscriminatorValue("P")
public class Professeur extends User {

	@Size(min = 1, max = 40, message = "{elem.nom}")
	@Column(length = 40)
	private String matricule ;

	public Professeur(String matricule) {
		super();
		this.matricule = matricule;
	}


}
