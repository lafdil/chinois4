package org.isfce.tfe.model;

public enum Roles {
	ROLE_ADMIN, ROLE_PROF, ROLE_ETUDIANT, ROLE_PERSONNEL_CUISINE
}
