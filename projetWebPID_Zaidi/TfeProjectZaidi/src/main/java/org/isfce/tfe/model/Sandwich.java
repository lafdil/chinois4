package org.isfce.tfe.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.isfce.tfe.controller.dto.SandwichDto;
import org.isfce.tfe.model.Etudiant;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@Data
@Entity(name = "TSANDWICH")
public class Sandwich {

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Integer id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PRICE")
	private double price;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "TSANDWICH_INGREDIENTS", joinColumns = @JoinColumn(name = "FKSANDWICH"), inverseJoinColumns = @JoinColumn(name = "FKINGREDIENT"))
	protected Set<Ingredient> ingredients = new HashSet<>();

	public Sandwich(Integer id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public  SandwichDto toDto() {
		SandwichDto sandwichDto = new SandwichDto(this.id, this.name, this.price);
		sandwichDto.setIngredients(this.ingredients);
		return sandwichDto;
	}

	/*
	 * public double calculatetotalPrice() {
	 * 
	 * return this.quantity * this.price; }
	 */

}