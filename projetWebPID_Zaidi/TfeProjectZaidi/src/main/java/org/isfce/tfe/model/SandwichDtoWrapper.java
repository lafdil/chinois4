package org.isfce.tfe.model;

import java.util.List;

import org.isfce.tfe.controller.dto.SandwichDto;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

//@Data
@NoArgsConstructor
@AllArgsConstructor

//SandwichDtoWrapper is a Java class that contains a list of SandwichDto objects

public class SandwichDtoWrapper {
	private List<SandwichDto> sandwichesDto;

	public List<SandwichDto> getSandwichesDto() {
		return sandwichesDto;
	}

	public void setSandwichesDto(List<SandwichDto> sandwichesDto) {
		this.sandwichesDto = sandwichesDto;
	}

	
	
	
	

	/*
	 * private double calculateTotalCost() { double total = 0; for (Sandwich
	 * sandwich : sandwiches) { total += sandwich.getPrice(); } return total; }
	 */
}
