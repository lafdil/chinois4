
package org.isfce.tfe.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// je peux ajouter le type DiscriminatorType..string..abstract because we do not want to create instances of User class
@DiscriminatorColumn(name = "USER_TYPE")
@Entity(name = "TUSER")
public abstract class User {

	@Id
	@Column(length = 50, nullable = false)
	private String username;

	// @Enumerated(EnumType.STRING)
	private Roles role;

	@Column(length = 100, nullable = false)
	@ToString.Exclude
	private String password;

	@Column(length = 30, nullable = false)
	private String prenom;

	@Email(message = "{email.nonValide}")
	@Column(length = 50, nullable = false)
	private String email;

	@Column(length = 30)
	private String tel;

	@Column(name = "CREDITBALANCE")
	private double creditBalance;
	/*
	 * public User(String username, Roles role, String password, String prenom,
	 * String email, String tel) {
	 * 
	 * this.username = username; this.role = role; this.password = password;
	 * this.prenom = prenom; this.email = email; this.tel = tel; }
	 */

	/*
	 * public User() { super(); }
	 */
	public void orderSandwich(double cost) {
		if (cost > creditBalance) {
			throw new IllegalArgumentException("Insufficient credit balance");
		}
		creditBalance -= cost;
	}
}
