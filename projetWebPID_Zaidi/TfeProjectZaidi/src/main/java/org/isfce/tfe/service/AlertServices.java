package org.isfce.tfe.service;

import org.isfce.tfe.dao.IAlertJpaDao;
import org.isfce.tfe.model.Alert;
import org.isfce.tfe.model.Ingredient;
import org.springframework.stereotype.Service;


@Service
public class AlertServices {
	
	private IAlertJpaDao iAlertJpaDao;
	
	
	public Alert save(Alert alert) {
		return iAlertJpaDao.save(alert);

	}

}
