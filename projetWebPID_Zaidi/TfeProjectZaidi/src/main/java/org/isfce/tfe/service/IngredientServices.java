package org.isfce.tfe.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.isfce.tfe.controller.exceptions.NotExistException;
import org.isfce.tfe.dao.IIngredientJpaDao;
import org.isfce.tfe.model.Ingredient;
import org.isfce.tfe.model.Sandwich;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class IngredientServices {

	@Autowired
	private IIngredientJpaDao ingredientJpaDao;

	public List<Ingredient> findAll() {
		return ingredientJpaDao.findAll();
	}

	/**
	 * Vérifie si un ingredient existe
	 * 
	 * @param id
	 * @return
	 */
	public boolean exists(int id) {

		return ingredientJpaDao.existsById(id);

	}

	public void insert(Ingredient ingredient) {

		ingredientJpaDao.save(ingredient);
	}

	/**
	 * retourne un cours à partir de son code
	 * 
	 * @param code
	 * @return un Optional de cours
	 */
	public Ingredient findOne(int id) {
		return ingredientJpaDao.findById(id).orElseThrow(() -> new NotExistException(id));
	}

	public Ingredient update(Ingredient ingredient) {
		// Check that the ingredient object is not null
		assert ingredient != null : "L'ingredient doit exister";
		// Save the updated ingredient to the database and return it
		return ingredientJpaDao.save(ingredient);
	}

	/**
	 * Suppression d'un ingredient s'il existe Il ne faut pas qu'il possède des
	 * liens vers...
	 * 
	 * @param code
	 */
	public boolean delete(int id) {
		if (id == 0)
			return false;
		ingredientJpaDao.deleteById(id);
		return true;

	}

	// count() is in crud repo, so we do not have to mention it in Icerticat repo
	public int getNumberOfIngredients() {

		return (int) (ingredientJpaDao.count());
	}

	public Ingredient save(Ingredient ingredient) {
		return ingredientJpaDao.save(ingredient);

	}

	}