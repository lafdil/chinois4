package org.isfce.tfe.service;

import java.util.List;
import java.util.Optional;

import org.isfce.tfe.controller.exceptions.NotExistException;
import org.isfce.tfe.dao.ISandwichJpaDao;
import org.isfce.tfe.model.Sandwich;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SandwichServices {
	
	@Autowired
	private ISandwichJpaDao SandwichJpaDao;
	
	@Transactional
	public List<Sandwich> findAll() {
		return SandwichJpaDao.findAll();

	}
	
	public Sandwich findOne(int id) {
		return SandwichJpaDao.findById(id).orElseThrow(()-> new NotExistException(id));
	}

	public Optional<Sandwich> getSandwichById(int sandwich) {
		return SandwichJpaDao.findById(sandwich);

	}
	
	public Sandwich insert(Sandwich sandwich) {
		return SandwichJpaDao.save(sandwich);
	}
	// count() is in crud repo, so we do not have to mention it in Icerticat repo

			
	
	public int getNumberOfSandwiches() {
		return (int) (SandwichJpaDao.count());
	}

}
