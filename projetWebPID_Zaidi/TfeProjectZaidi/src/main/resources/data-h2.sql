
MERGE INTO TUSER (USER_TYPE, USERNAME, ROLE, PASSWORD, PRENOM, EMAIL, TEL, CREDITBALANCE, NUM_ETUDIANT, MATRICULE) VALUES

('E', 'et1', 2, '$2a$10$N8pzEKs1350SPT1vpomUo.zMEV1IR/LjXgOrBJ.QaeddZtKLVxR4q', 'Didier', 'john.doe@example.com', '555-1234',100, 'etuduant1', null),
('P', 'prof1', 1, '$2a$10$PdX74d5AzJN/OVU3bP1XbO3fLSfr6x3oKC81rbiUAlSEE6DcID986', 'Stephane', 'Stephane@example.com', '666-1234', 100, null, 'professeur1');




MERGE INTO TINGREDIENT (ID, NAME_INGREDIENT, EXPIRATION_DATE, STOCK_QTY, THRESHOLD, MIN_QTY, PRICE_OF_MINI_QTITY) VALUES
(1, 'Filet Américain', '2023-03-31', 4000, 2000,50,1),
(2, 'Gouda Cheese', '2023-04-15', 6000, 2000, 20, 80),
(3, 'Sliced Turkey Breast', '2023-04-30', 10000, 2000, 50, 1.5),
(4, 'Tiger Bread', '2023-04-30', 100, 22, 1, 0.99),
(5, 'Chicken', '2023-05-02',15000, 4000,50, 1.5),
(6, 'Meatballs', '2023-05-04',15000, 4000, 1,2),
(7, 'Tuna', '2024-05-04', 500, 150, 1, 1);

	
MERGE INTO TSANDWICH (ID,name, price) VALUES
(1, 'Cheese and Tomato', 4),
(2, 'Tuna', 5),
(3,'Chicken Sandwich', 5);



--Dans le sandwich 1, on a 50g de Filet Américain et il y a 1 Tiger Bread
--here i did that similar to l'inscription des etudiants dans un module

MERGE INTO TSANDWICH_INGREDIENTS (ID, FKSANDWICH, FKINGREDIENT, QUANTITY) VALUES
(1, 1, 1, 50),
(2, 1, 4, 1),
(3, 2, 7, 1),
(4, 2, 4, 1),
(5, 3, 4, 1);

MERGE INTO TCOMMANDE VALUES
(1, '2023-11-28 15:30:14', 'et1');

--MERGE INTO TSANDWICH (ID,FKINGREDIENT, NAME_INGREDIENT, DATE_ALERT, STOCK_QTY) VALUES
--(1, 4, 'Tiger Bread', '2022-04-30', 20 );

