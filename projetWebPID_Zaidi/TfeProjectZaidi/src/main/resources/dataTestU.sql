MERGE INTO TUSER VALUES
('admin','$2a$10$BR98DmjHecYVdqtNz3UGcuyFop/ed0KfxOEAp1bdVXh8eF2iAfOz.','0' ),
('vo', '$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq','1'),
('wa','$2a$10$ZZ4U5deuZBQqp/BNmKrG1e6OqiRwmaAfu.aaQfgBI9ehImH8AYZKC','1'),
('sh','$2a$10$PdX74d5AzJN/OVU3bP1XbO3fLSfr6x3oKC81rbiUAlSEE6DcID986',1),
('et1','$2a$10$N8pzEKs1350SPT1vpomUo.zMEV1IR/LjXgOrBJ.QaeddZtKLVxR4q',2),
('et2','$2a$10$.Tlzs3a.2PEGK1gckhgaJuz4SWW2J1lWBf.4E3rOleziGDpExtp1W',2),
('et3','$2a$10$eeilUxVqPB8dXMsnzxiIp.OlOyD1isPtICWaRRL5VFofqcWcSURk2',2),
('et4','$2a$10$35yXfuSOyZkLLXFKXQbDLuzvdepJA2cFwxqu.VOsDuuAgf5ujVOuu',2),
('et5','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',2),
('sec1','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',3),
('sec2','$2a$10$WvAj8iSJyIxDQWHysC4o6usLkzKW5bi9GThqYH2X/w/vVAm18nWcq',3);

MERGE INTO TPROFESSEUR(ID,NOM,PRENOM,EMAIL,FKUSER) VALUES
(1,'Van Oudenhove','Didier','vo@isfce.be','vo'),
(2,'Wafflard','Alain','waff@isfce.be','wa'),
(3,'Huguier','Stephane','sh@isfce.be','sh');
--Réinitialise le compteur identity 
alter table TPROFESSEUR ALTER COLUMN ID RESTART WITH (select max(id)+1 from tprofesseur);


MERGE INTO TETUDIANT (ID,NOM,PRENOM,EMAIL,FKUSER,TEL) VALUES
(1 , 'Nom_ET1', 'Prenom_ET1' , 'et1@isfce.be', 'et1', '02/800.00.01'),
(2 , 'Nom_ET2', 'Prenom_ET2', 'et2@isfce.be', 'et2', '02/800.00.02'),
(3 , 'Nom_ET3', 'Prenom_ET3', 'et3@isfce.be', 'et3', '02/800.00.03'),
(4 , 'Nom_ET4', 'Prenom_ET4', 'et4@isfce.be', 'et4', '02/800.00.04');
--Réinitialise le compteur identity 
alter table TETUDIANT ALTER COLUMN ID RESTART WITH (select max(id)+1 from TETUDIANT);