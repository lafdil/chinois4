package org.isfce.tfe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TfeProjectZaidiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TfeProjectZaidiApplication.class, args);
	}

}
