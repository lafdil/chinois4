//import javax.swing.JOptionPane;
public class Plat {
   private String nom ;
   private float prix ;
   protected int stock ;                             
   public Plat(String nom, float prix, int stock) {
      this.nom = nom;
      this.prix = prix;
      this.stock = stock;
   }
   public String getNom() {
      return nom;
   }
   public float getPrix() {
      return prix;
   }
   public void diminuer(int qte) throws ErrStock { 
      if (qte > stock) {
            
         throw new ErrStock(stock, qte) ;          
      }
      else {
         stock-=qte ;
      }
   }
    
    
    
    
    
 /*   public boolean diminuer(int qte) {
        boolean commandeValidee = false ;
        try {
            commandeValidee = essaiDiminuer(qte);
        } catch (ErrStock ex) {}
        return commandeValidee ;
    }
    private boolean essaiDiminuer(int qte) throws ErrStock {
        boolean commandeOk = false ;
        if (qte > stock) {
            
            throw new ErrStock(stock, qte) ;          
        }
        else {
            commandeOk = true ;
            stock-=qte ;
        }
        return commandeOk ;
    }
    */
}