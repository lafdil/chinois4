public class PlatFaitMaison extends Plat {
    public PlatFaitMaison(String plat, float prix, int qte) {
        super(plat, prix, qte) ;                        
    }  
  
    public void ajouter(int qteSuppl) {
        stock += qteSuppl ;
        System.out.println(qteSuppl+" "+this.getNom()+" en supplement");
    }
}
