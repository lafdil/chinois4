import java.util.*;
public class TestTraiteurChinois2 {
   public static void main(String[] args) {
      float total = 0 ;
      final int NB = 3 ;                                
      Plat tab[] = new Plat[NB] ;                            
      tab[0] = new PlatToutFait("Poulet aigre-doux",10,8);   
      tab[1] = new PlatFaitMaison("Nouilles sautees aux scampis",9,3);
      tab[2] = new PlatFaitMaison("Riz aux mille delices",10.5f,5);
      // Affichage de la carte
      System.out.println("   LA CARTE");
      for(int i=0 ; i<tab.length ; i++) {
         System.out.println((i+1)+" - "+tab[i].getNom()+" - "
                +tab[i].getPrix()+" - "+tab[i].stock+ " unites");
      }
      // Prise de commande
      System.out.println("   LA COMMANDE");
      int choix ;
      String x = "";
      do {
         do {
            System.out.print("Num de plat de 1 a "+tab.length+" ?   ");
            Scanner input = new Scanner(System.in);
                      
            choix = input.nextInt();
         
            //choix = Clavier.lireInt();
         }  while(choix<1 || choix>tab.length);              
         System.out.print(tab[choix-1].getNom()+           
                " en combien d'unites ?   ");
                
         Scanner input = new Scanner(System.in);
                      
         int combien = input.nextInt();
      
         //int combien = Clavier.lireInt();
         try{
            tab[choix-1].diminuer(combien);
         }
         catch(ErrStock e){}  
         total += tab[choix-1].getPrix() * combien ; 
         System.out.println(total);
      
         System.out.print("Voulez-vous encore commander (Y/N) ?   ");
         Scanner input2 = new Scanner(System.in);
                      
         x = input2.nextLine();
      
      } while(x.equalsIgnoreCase("Y"));
      //while(Clavier.lireString().equalsIgnoreCase("Y"));        
      System.out.println(total + " ");
      // Prepparation de 5 plats faits maison
      ((PlatFaitMaison)tab[1]).ajouter(6);                
      // Affichage du stock restant
      System.out.println("Stock restant");
      for(int i=0 ; i<tab.length ; i++) {
         System.out.println((i+1)+" - "+tab[i].getNom()+" - "+tab[i].getPrix()+" - "+tab[i].stock+ " unites");
      }
   }
}