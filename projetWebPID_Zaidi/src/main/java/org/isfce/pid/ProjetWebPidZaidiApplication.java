package org.isfce.pid;

import org.isfce.pid.model.Professeur;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetWebPidZaidiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetWebPidZaidiApplication.class, args);
	}

}
