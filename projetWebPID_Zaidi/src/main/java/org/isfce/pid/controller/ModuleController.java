package org.isfce.pid.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.isfce.pid.service.CoursServices;
import org.isfce.pid.service.ModuleServices;
import org.isfce.pid.service.ProfesseurServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

import org.isfce.pid.controller.dto.ModuleDto;
import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.model.Module;
import org.isfce.pid.model.Module.MAS;

/*lafdil 1h55 qd on a un controlleur il faut une adresse root ici pour la classe
donc on va faire un request mapping, /module c est l'URL de base
un logueur pour tester c est la 2eme video 52 min
*/
@Slf4j
@Controller
@RequestMapping("/module")
public class ModuleController {
	// lafdil le controlleur communique avec la couche de service et pas directement
	// avec les DAO;
	ModuleServices moduleServices;
	CoursServices coursServices; // lafdil 1ere video 13-1Dec on va se faire injecter CoursServices--pour voir si
									// un cours exist, recharger un cours
	ProfesseurServices professeurServices; // lafdil 13Dec-video1--1h12pour recuperer la liste des codes de tous les
											// profs

	@Autowired
	public ModuleController(ModuleServices moduleServices, CoursServices coursServices,
			ProfesseurServices professeurServices) {

		this.moduleServices = moduleServices;
		this.coursServices = coursServices;
		this.professeurServices = professeurServices;
	}

//6Dec video de 3h---2h45--lafdil - liste des modules pour un prof specifie and we are going to bind it to Optional<String> oprof
	// the variable that we have passed in the URI --{prof}---is binded to oprof
	@GetMapping({ "/liste/{prof}", "/liste" })
	String listeModule(@PathVariable("prof") String oprof, Model model) {

		List<Module> liste = new ArrayList<>();
		if (oprof != null){

			liste = moduleServices.getModulesProf(oprof);
			System.out.println(liste.size()+"/////////////");
		}
		else{

			liste = moduleServices.findAll();
		}
		System.out.println(liste.size()+"/////////////");
		model.addAttribute("liste", liste);
		return "module/listeModules";
	}


/*	@GetMapping({ "/liste" })
	String listeModules(@RequestParam("prof") String profCode,@RequestParam("cours") String coursCode, Model model) {
		List<Module> liste;
		if (profCode != null && coursCode != null )
			liste = moduleServices.getModulesProf(profCode);
		else
			liste = moduleServices.findAll();
		model.addAttribute("liste", liste);
		return "module/listeModules";
	}*/

//	6Dec video de 3h---2h40--{code } c est  a dire un parametre de l’URL, qd on clique sur poubelle, le code apparait dans URI
	@PostMapping("/{code}/delete")
	public String deleteModule(@PathVariable String code) {
		moduleServices.delete(code);
		return "redirect:/module/liste";
	}

	/*
	 * ce ci se trouve dans add module.html--th:action="@{'/module/' +
	 * ${moduleDto.cours} + '/add'}" Video1 17jan---26min
	 */ @GetMapping("{cours}/add")
	String addModuleGet(@PathVariable String cours, Model model) {

		/*
		 * lafdil 13Dec-video1--45min verifie si un cours exist, et je lui donne le code
		 * du cours en haut, s' il n existe pas on genere l' exception lafdil
		 * NotExistException est deja catche par notre controlleur advice et qui envoie
		 * une page d' erreur le if --Vérifie si un cours existe
		 */

		if (!coursServices.exists(cours))
			throw new NotExistException(cours);
		// Obtenir le code du module moduleServices.generateCodeModule(cours);
		// ajout DTO

		String codeModule = moduleServices.GenerateCodeModule(cours);

		// j ai mon DTO
		ModuleDto dto = new ModuleDto(codeModule, cours);
		model.addAttribute("moduleDto", dto);

		/*
		 * ajout MAS lafdil 13Dec-video1--1h10min they have a static values method that
		 * returns an array containing all of the values of the enum in the order they
		 * are declared. values() method can be used to return all values present inside
		 * the enum. j'ajoute la liste des moments
		 */
		model.addAttribute("mas", MAS.values());
		/*
		 * ajout list des username des profs, et on demande a un serviceprofesseur
		 * d'avoir ca--lafdil --renvoie an array de type enumere. getListProfUsername
		 * j'ajoute la liste des codes des professeurs
		 */ model.addAttribute("profs", professeurServices.getListProfUsername());

		// zaidi ceci nous retourne la page addModule qui est dans module et ensuite
		// template
		return ("module/addModule");

	};

	/*
	 * lafdil 49min 13-2dec--qu' est ce qu' on va recuperer au niveau de la methode
	 * post et apres une redirection vers la liste des modules, je dois recuperer l'
	 * l'objet qui a ete cree. pour etre sur qu' il rajoute dans la map
	 * (@ModelAttribute) et the @ModelAttribute annotation binds the form fields to
	 * the moduleDTO object. en plus moduleDTO doit etre valide (par @Valid)qui
	 * permettra la validation de toutes les qu'on a mis dans la classe moduleDTO il
	 * devra mon reconstruire mon DTO
	 * 
	 * @Valid==va valider toutes les annotations qu on a mis sur la classe DTO et
	 * les erreurs on les mets dans l' objet errors du BindingResult log.info-- il
	 * va afficher un log avec toutes les informations du module
	 * zaidi @ModelAttribute--bind form data to a java object, this method will
	 * handle http Post request that is sent to--{ cours}/add, @ModelAttribute--
	 * reads form data,  pour dire que cest un parametre de l'URL{cours} 
	 */
	@PostMapping("{cours}/add")
	String addModulePost(@ModelAttribute("moduleDto") @Valid ModuleDto moduleDto, BindingResult errors, Model model) {
		log.info(" Post ajout d' un module: " + moduleDto);
		// Gestion de la validation en fonction des annotations
		// lafdil hasErrors()---boolean--Return if there were any errors.
		if (errors.hasErrors()) {
			log.debug("Erreurs dans les données du module:" + moduleDto.getCode());
			// ajout MAS, values() method can be used to return all values present inside
			// the enum.
			model.addAttribute("mas", MAS.values());
			// ajout list des username des profs, et on demande a un serviceprofesseur
			// d'avoir ca
			model.addAttribute("profs", professeurServices.getListProfUsername());
			return ("module/addModule");
		}
		// on cree un objet module
		Module module = moduleDto.toModule(coursServices, professeurServices);
		// sauve en BD
		moduleServices.insert(module);

		return "redirect:/module/liste";

	}

}
