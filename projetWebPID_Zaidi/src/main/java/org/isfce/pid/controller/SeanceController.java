package org.isfce.pid.controller;


import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;
import org.isfce.pid.controller.dto.CoursDto;
import org.isfce.pid.controller.dto.EtudiantPresenceDto;
import org.isfce.pid.controller.dto.ModuleDto;

import org.isfce.pid.controller.dto.SeanceDto;
import org.isfce.pid.controller.exceptions.NotExistException;
import org.isfce.pid.controller.wrapper.PresenceWrapper;
import org.isfce.pid.enums.PresenceEnum;
import org.isfce.pid.model.*;
import org.isfce.pid.model.Module;
import org.isfce.pid.model.Module.MAS;
import org.isfce.pid.service.CertificatService;
import org.isfce.pid.service.ModuleServices;
import org.isfce.pid.service.PresenceServices;
import org.isfce.pid.service.SeanceServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@RequestMapping("/seance")
public class SeanceController {

    @Autowired
    SeanceServices seanceServices;

    @Autowired
    ModuleServices moduleServices;

    @Autowired
    PresenceServices presenceServices;

    @Autowired
    CertificatService certificatService;

    // This variable just to get a moduleCode for current module
    private String currentModule;


    // video1--13dec--50min@GetMapping("/{module}/add")---je dois egalemen avoir une
    // liste des seances

    @GetMapping("/{module}/liste")
    public String showListeSeances(@PathVariable(name = "module") String module, Model model) {
        List<Seance> liste;
        liste = seanceServices.getSeancesModule(module);
        model.addAttribute("liste", liste);
        System.out.println("============");
        // Change value for currentModule varable
        this.currentModule = module;
        return ("seance/listeSeances");
    }

    @GetMapping("/addSeance")
    public String AddSeanceGet(Model model){

        // j ai mon DTO
        SeanceDto dto = new SeanceDto(null,0, LocalDate.now(),currentModule,false);
        model.addAttribute("seanceDto", dto);

        return ("seance/addSeance");
    }
    @PostMapping("/addSeance")
    public String addSeancePost(@ModelAttribute("seanceDto") @Valid SeanceDto seanceDto, BindingResult errors, Model model){

        log.info(" Post ajout d' une seance: " + seanceDto);

        if(errors.hasErrors()){
            log.debug("Erreurs dans les données du seance:" + seanceDto.getCode());
            // Show error to user in the template
            return ("seance/addSeance");
        }else{
            // Mapping seanceDto to seance
            Seance seance = seanceDto.toSeance(moduleServices);

            // Save seance into database
            Seance savedSeance = seanceServices.insert(seance);

            // Get all students registered in current module
            Set<Etudiant> etudiantList = savedSeance.getModule().getEtudiants();

            // Create a new presence for each student in our list and save all presence in one list
            List<Presence> presenceList = new ArrayList<>();

            etudiantList.forEach( e -> {
                PresencePkId presencePkId = new PresencePkId(seance.getCode() , e.getId());
                // Search if a student has a cerificat valid for thie seance
                boolean isStudentHasValidCertificat = certificatService.checkIfStudentHasValidCertificat(savedSeance.getSeanceDate(),e);
                Presence presence;

                if (isStudentHasValidCertificat){
                    presence = new Presence(presencePkId, seance,e, PresenceEnum.CM);
                }else {
                    presence = new Presence(presencePkId, seance,e, null);
                }


                presenceList.add(presence);
            });

            // Save presenceList to PRESENCE table
            presenceServices.insertPresenceList(presenceList);

            return "redirect:/seance/"+seanceDto.getModuleCode()+"/liste";




        }
    }

    @GetMapping("/{seanceCode}/info")
    public String getSeanceInfo(@PathVariable String seanceCode, Model model){

        // Get seance from database
        Seance seance = seanceServices.getSeanceById(seanceCode);

        // Get list presence for this seance
        List<Presence> presenceList = presenceServices.getPresenceListBySeance(seance.getCode());
        presenceList.forEach(e->System.out.println(e.getJustificatif()+"-------"));

        // Create wrapper presence
        PresenceWrapper presenceWrapper = new PresenceWrapper();
        presenceWrapper.setPresenceListe(new ArrayList<>(presenceList.stream().sorted(Comparator.comparing(p->p.getEtudiant().getNom())).collect(Collectors.toList())));
        presenceWrapper.setSeanceDate(seance.getSeanceDate());

        presenceWrapper.setIsCloture(seance.getIsCloture());

        // Get module from seance
        Module module = seance.getModule();

        // Merge presenceList and studentsList into EtudiantPresenceDto




        // Get students list from module
        /*Set<Etudiant> etudiants = module.getEtudiants().stream()
                .sorted(Comparator.comparing(Etudiant::getNom)).collect(Collectors.toSet());*/






        // Send all info to the model
        model.addAttribute("seance",seance);
        model.addAttribute("module",module);
        model.addAttribute("wrapperPresence", presenceWrapper);

        return ("/seance/infoSeance");

    }

    @PostMapping("/{seanceCode}/presences")
    public String updateSeanceAndPresences(@PathVariable String seanceCode,@ModelAttribute("presenceWrapper") PresenceWrapper presenceWrapper, Model model){




            // Find a seance with seance code and change isCloture and date if the client change the date
            Seance currentSeance = seanceServices.getSeanceById(seanceCode);

            currentSeance.setIsCloture(presenceWrapper.getIsCloture());
            currentSeance.setSeanceDate(presenceWrapper.getSeanceDate());



            // Save the seance but in this case just we'll be updated
            seanceServices.insert(currentSeance);


            // Now we need to check presence status for each studiant and save all updates
            List<Presence> presenceList = presenceServices.getPresenceListBySeance(currentSeance.getCode());

            for( int index =0; index < presenceList.size(); index++ ){
                presenceList.get(index).setJustificatif(presenceWrapper.getPresenceListe().get(index).getJustificatif());
            }
            presenceServices.insertPresenceList(presenceList);











        return ("redirect:/seance/"+currentModule+"/liste");

    }




}
