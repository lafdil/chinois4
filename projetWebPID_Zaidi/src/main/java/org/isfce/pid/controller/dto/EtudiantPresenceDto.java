package org.isfce.pid.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.isfce.pid.model.Etudiant;

@Data
public class EtudiantPresenceDto {

    private Etudiant etudiant;
    private boolean presenceStatut;

}
