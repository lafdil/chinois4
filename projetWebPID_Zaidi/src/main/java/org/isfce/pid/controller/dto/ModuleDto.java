package org.isfce.pid.controller.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.isfce.pid.model.Module;
import org.isfce.pid.model.Module.MAS;
import org.isfce.pid.service.CoursServices;
import org.isfce.pid.service.ProfesseurServices;
import org.isfce.pid.util.validation.annotation.DatesPastAndFutureValidation;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;
/*lafdil 29min--13dec... on a pas mis @Data parceque on a pas besoin de ToString mais uniquement 
des Getters et des Setters*/

@Getter
@Setter
//@DatesPastAndFutureValidation(d1 = "dateDebut", d2 = "dateFin", message = "{date.compare}")
public class ModuleDto {

	@Pattern(regexp = "[A-Z0-9]{3,8}-[0-9]-[A-Z]", message = "{elem.code}")

	private String code;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")

	private LocalDate dateDebut;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateFin;

	// lafdi on est oblige de preciser un moment
	@NotNull
	private MAS moment;

	// lafdil c est le code du cours et la meme chose pour prof a la ligne 41

	@NotNull
	private String cours;

	@NotNull
	private String professeur;

	// lafdil 33min ---13Dec--uniquement le code du module et le cours

	public ModuleDto(String code, String cours) {

		this.code = code;
		this.cours = cours;
	}

	public Module toModule(CoursServices coursServices, ProfesseurServices professeurServices) {

		return new Module(getCode(), getDateDebut(), getDateFin(), getMoment(), coursServices.findOne(getCours()).get(),
				professeurServices.getByUserName(getProfesseur()).get(), null);
	}
	
	
	

}
