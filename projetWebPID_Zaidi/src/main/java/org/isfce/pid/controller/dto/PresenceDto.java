package org.isfce.pid.controller.dto;

import org.isfce.pid.model.Presence;
import org.isfce.pid.model.PresencePkId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PresenceDto extends JpaRepository<Presence, PresencePkId> {
}
