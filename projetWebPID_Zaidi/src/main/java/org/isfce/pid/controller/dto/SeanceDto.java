package org.isfce.pid.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.isfce.pid.model.Seance;
import org.isfce.pid.service.ModuleServices;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data

@AllArgsConstructor
public class SeanceDto {

    private String code;

    private int duree;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate seanceDate;


    private String moduleCode;

    private Boolean isColture;


    public Seance toSeance(ModuleServices moduleServices){
        return new Seance( getCode(), getDuree(),getSeanceDate(), moduleServices.getModuleById(moduleCode).get(),false );
    }

}
