package org.isfce.pid.controller.wrapper;

import lombok.Data;
import org.isfce.pid.model.Presence;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class PresenceWrapper {

    private ArrayList<Presence> presenceListe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate seanceDate;

    private boolean isCloture;

    public boolean getIsCloture() {
        return isCloture;
    }

    public void setIsCloture(boolean cloture) {
        isCloture = cloture;
    }
}

