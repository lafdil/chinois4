package org.isfce.pid.dao;

import java.util.Optional;

import org.isfce.pid.model.Professeur;
import org.springframework.data.jpa.repository.Query;

public interface IEtudiantJpaDao {

	@Query("select count(*)=1 from TUSER u where u.username=?1 and u.role=2")
	boolean existByUserName(String username);
	
	@Query("from TETUDIANT e join e.user u where u.username=?1")
	Optional<Professeur> findByUsername(String username);

}
