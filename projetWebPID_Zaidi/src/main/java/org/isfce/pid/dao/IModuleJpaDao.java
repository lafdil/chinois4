package org.isfce.pid.dao;

import java.util.List;
import java.util.Optional;

import org.isfce.pid.model.Cours;
import org.isfce.pid.model.Module;
import org.isfce.pid.model.Module.MAS;
import org.isfce.pid.model.Professeur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IModuleJpaDao extends JpaRepository<Module, String> {
	
	//1. liste des modules pour un cours, triée sur l’attribut Moment (en utilisant le mini-dsl)
    List<Module> findByCoursOrderByMoment(Cours cours);
    
    
    //2. liste de tous les modules pour une section et un moment (MAS), en utilisant un query JPA ou
    //natif
    // query natif
    @Query(value="SELECT DISTINCT m.CODE FROM TMODULE m join TCOURS c join TSECTION s where m.moment = ?2 and s.section = ?1", nativeQuery=true)
    List<Module> getModulesSection2(String section, MAS jour);

    // query JPA
    @Query("from TMODULE m join m.cours where m.moment = ?2 and ?1 member of m.cours.sections ")
    List<Module> getModulesSection(String section, MAS jour);

    List<Module> findModuleByProfesseurOrderByCode(Professeur professeur);

	/*
	 * lafdil video6dec -40min 3. liste de tous les modules enseignés par le professeur spécifié. La liste doit
	 * être triée sur le code du module--on utilise pas le vocabulaire de jpa et pas de la base de donnees --ex le nom de la base de donnees
	 */
    @Query("FROM TMODULE m where m.professeur.user.username = ?1 order by m.code")
    List<Module> getModuleProfesseur(String username);
    
// 4. créez la méthode qui génère le code du module en utilisant un query natif.
//    lafdil 1er video 13/12- 58 min, the || operator concatenates strings
    @Query(value="SELECT ?1 || '-' || (count (*) + 1) || '-A' from TMODULE m where m.fkcours= ?1  ", nativeQuery=true)
    String generateCodeModule(String cours);


	//Optional<Module> findOne(String code);
    
    
    //List<Module> findByCode(String code);
}
