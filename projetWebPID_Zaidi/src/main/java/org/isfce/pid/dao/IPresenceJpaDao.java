
  package org.isfce.pid.dao;
  
  import org.isfce.pid.model.Presence; import org.isfce.pid.model.PresencePkId;
  import org.isfce.pid.model.Seance; import
  org.springframework.data.jpa.repository.JpaRepository;
  
  public interface IPresenceJpaDao extends JpaRepository <Presence, PresencePkId>{
  
  }
 