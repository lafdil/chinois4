package org.isfce.pid.dao;

import java.util.List;
import java.util.Optional;

import org.isfce.pid.model.Professeur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IProfesseurJpaDao extends JpaRepository<Professeur, Integer> {

	/**
	 * « public Optional<Professeur> getByUserName(String username) » qui retourne
	 * un optional de professeur via son username.
	 * 
	 * @param username
	 * @return
	 */

	@Query("from TPROFESSEUR p join p.user u where u.username=?1")
	Optional<Professeur> findByUsername(String username);

	/**
	 * lafdil 1h15 et 1h23- 13Dec-video1
	 * « List<String> getListProfUsername() » qui retourne la liste 
	 * des « username » de tous les professeurs triée par « username ».
	 	 */

	@Query("SELECT u.username from TUSER u where u.role=1 order by u.username ")
	List<String> ListProfUserName();
	
		
	@Query("select count(*)=1 from TUSER u where u.username=?1 and u.role=1")
	boolean existByUserName(String username);
	
	int countByNomAndPrenom(String nom, String prenom);


	

}
