
  package org.isfce.pid.dao;
  
  import org.isfce.pid.model.Seance; import
  org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

  import java.util.List;

  public interface ISeanceJpaDao extends JpaRepository<Seance, String>{


      /*@Query(value = "SELECT ?1 || '-' || 's'|| (count (*) + 1) from TSeance s where s.FKMODULE= ?1", nativeQuery = true)
      String generateCodeSeance(String module);*/

      @Query("FROM TSEANCE s where s.module.code = ?1 order by s.code")
      List<Seance> getSeancesModule(String codeModule);


		 

  
  }
