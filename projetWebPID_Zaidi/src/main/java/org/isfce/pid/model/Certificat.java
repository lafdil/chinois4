package org.isfce.pid.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.isfce.pid.util.validation.annotation.DatesPastAndFutureValidation;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TCERTIFICAT")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@DatesPastAndFutureValidation(d1 = "dateDebut", d2 = "dateFin", message = "{date.compare}")
public class Certificat {
	
	
	

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(nullable = false,name = "DATEDEBUT")
    private LocalDate dateDebut;


    @Column(nullable = false, name = "DATEFIN")
    private LocalDate dateFin;


    @ManyToOne
    @JoinColumn(name="ETUDIANT_ID")
    private Etudiant etudiant;

    private String nom_Medecin;
}
