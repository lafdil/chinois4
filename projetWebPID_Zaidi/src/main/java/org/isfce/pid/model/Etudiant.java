
package org.isfce.pid.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name = "TETUDIANT")
public class Etudiant {

	@Id
	// Id généré par la base de données
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter // pas de setter
	private Integer id;

	@Getter
	@OneToOne(optional = false, cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
	@JoinColumn(name = "FKUSER", unique = true, nullable = false, updatable = false)
	private User user;

	@NotNull
	@Size(min = 1, max = 40, message = "{elem.nom}")
	@Column(length = 40, nullable = false)
	private String nom;

	@Column(length = 30)
	private String prenom;

	@Email(message = "{email.nonValide}")
	@NotNull
	@Column(length = 50, nullable = false)
	private String email;

	@Column(length = 30)
	private String tel;

	public Etudiant(String nom, String prenom, String email, User user, String tel) {
		this(null, nom, prenom, email, user, tel);
	}

	public Etudiant(Integer id, String nom, String prenom, String email, User user, String tel) {
		assert (user.getRole() == Roles.ROLE_ETUDIANT);
		this.id = id;
		this.user = user;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.tel = tel;
	}

}
