
package org.isfce.pid.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@NoArgsConstructor(access = AccessLevel.PROTECTED)

@AllArgsConstructor

@Entity(name = "TPRESENCE")

public class Presence {

	// lafdil Attribut maintenant l'ID composé. That is a separate class that
	// contains the PK information

	public Presence(Seance seance, Etudiant etudiant, ETAT_PRESENCE justificatif) {
		
		this.seance = seance;
		this.etudiant = etudiant;
		this.justificatif = justificatif;
	}

	@EmbeddedId
	private PresencePkId id = new PresencePkId();

	@ManyToOne

	@JoinColumn(name = "FKSEANCE", insertable = false, updatable = false)
	private Seance seance;

	@ManyToOne

	@JoinColumn(name = "FKETUDIANT", insertable = false, updatable = false)
	private Etudiant etudiant;

	@Enumerated(EnumType.STRING)
	private ETAT_PRESENCE justificatif;

}
