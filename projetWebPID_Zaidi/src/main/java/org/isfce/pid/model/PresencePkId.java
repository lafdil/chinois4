
  package org.isfce.pid.model;
  
  import java.io.Serializable;
  
  import javax.persistence.Column; import javax.persistence.Embeddable; import
  javax.persistence.EmbeddedId; import javax.persistence.Entity; import
  javax.persistence.JoinColumn; import javax.persistence.ManyToOne;
  
  import lombok.AccessLevel; import lombok.AllArgsConstructor; import
  lombok.Data; import lombok.NoArgsConstructor;
  
  @Embeddable
  
  @Data
  
  @NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
  
  @AllArgsConstructor
  
  public class PresencePkId implements Serializable {
  
  @Column(name = "FKSEANCE") private final String seance;
  
  @Column(name = "FKETUDIANT") private final Integer etudiant;

}
  
  
  
 