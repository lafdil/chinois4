package org.isfce.pid.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@NoArgsConstructor(access = AccessLevel.PROTECTED)

@AllArgsConstructor

@Entity(name = "TSEANCE")
public class Seance {

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 16)
	private String code;

	private int duree;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateSeance;

	@ManyToOne
	@JoinColumn(name = "FKMODULE")
	private Module module;

}
