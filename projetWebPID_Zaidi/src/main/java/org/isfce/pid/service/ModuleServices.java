package org.isfce.pid.service;

import java.util.List;
import java.util.Optional;

import org.isfce.pid.dao.IModuleJpaDao;
import org.isfce.pid.dao.IProfesseurJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.isfce.pid.model.Cours;
import org.isfce.pid.model.Module;

/**
 * lafdil 1h49
 * 
 * @author Zaidi
 *
 */

@Service
public class ModuleServices {
	private IModuleJpaDao daoModule;

	@Autowired
	public ModuleServices(IModuleJpaDao moduledao, IProfesseurJpaDao daoProfesseur) {
		this.daoModule = moduledao;

	}

	/**
	 * lafdil 6Dec video1--26min
	 * 
	 * @return liste des modules triée par code et moment
	 */

	public List<Module> findAll() {
		return daoModule.findAll(Sort.by("code", "moment"));

	}

	// lafdil 1h42
	/**
	 * 
	 * @return liste des modules d'un professeur spécifié par son username
	 * 
	 */
	public List<Module> getModulesProf(String username) {
		return daoModule.getModuleProfesseur(username);

	}

	/**
	 * lafdil 1h47--Cours 6Dec video1
	 * supprime le module specifie
	 * 
	 * @Exception CanNotDeleteException et deviation sur une page d'erreur si le
	 *            module possede des inscriptions
	 */
	
	public void delete(String code) {
		daoModule.deleteById(code);

	}

	/**
	 * generer le prochain cours d' un module, ne verifie pas si le cours exist
	 * 
	 * @param cours le code du cours
	 * @return le prochain code d' un module pour un cours lafdil 50min-
	 *         V1-13-1Dec--qd je fait le post que ca sera sauvé
	 */

	public String GenerateCodeModule(String cours) {

		return daoModule.generateCodeModule(cours);
	}
	
	/**
	 * 
	 * @param module
	 * @returnle module sauvé
	 */

	public Module insert(Module module) {
		return daoModule.save(module);
	}

	

	// 2. exist(String code) » qui permet de savoir si un code de module existe.
	public boolean exist(String code) {
		return daoModule.existsById(code);

	}
	public Optional<Module> getModuleById(String id) {
		return daoModule.findById(id);
	 }
	public Optional<Module> findOne(String code) {
		return daoModule.findById(code);
	}

	// 3. « findOne(String code) » retourne un optional de module.

	/*
	 * public Optional<Module> getModule1(String id) {
	 * 
	 * return daoModule.findById(id); }
	 * 
	 * public Optional<Module> getModule2(String code) {
	 * 
	 * return daoModule.findOne(code); }
	 */

}
