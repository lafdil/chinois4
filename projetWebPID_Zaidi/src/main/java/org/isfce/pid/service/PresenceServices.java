package org.isfce.pid.service;

import java.util.List;

import javax.transaction.Transactional;

import org.isfce.pid.dao.IPresenceJpaDao;
import org.isfce.pid.dao.ISeanceJpaDao;
import org.isfce.pid.model.Presence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class PresenceServices {
	@Autowired
	private IPresenceJpaDao daoPresence;
	
	@Transactional
	public void insertListPresence(  List<Presence> presenceList) {
		
		daoPresence.saveAll(presenceList);
		
		
	}

}
