package org.isfce.pid.service;

import java.util.List;

import org.isfce.pid.dao.IModuleJpaDao;
import org.isfce.pid.dao.IProfesseurJpaDao;
import org.isfce.pid.dao.ISeanceJpaDao;
import org.isfce.pid.model.Module;
import org.isfce.pid.model.Seance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SeanceServices {

    private ISeanceJpaDao daoSeance;
    IModuleJpaDao moduleJpaDao;

    @Autowired
    public SeanceServices(ISeanceJpaDao daoSeance, IModuleJpaDao moduledao) {
        this.daoSeance = daoSeance;

    }

    public List<Seance> getSeancesModule(String codeModule) {

        return daoSeance.getSeancesModule(codeModule);

    }

    public List<Seance> findAll() {
        return daoSeance.findAll(Sort.by("code"));
        // return daoSeance.findAll();
    }

    public Seance insert(Seance seance) {
        return daoSeance.save(seance);
    }
    
    public void deleteSeanceById(String code) {
		this.daoSeance.deleteById( code);
		

	}

	
	
   /* public String GenerateCodeSeance(String module) {

        return  daoSeance.generateCodeSeance(module);
    }*/
}
