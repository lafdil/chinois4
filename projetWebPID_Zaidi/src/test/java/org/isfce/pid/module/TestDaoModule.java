package org.isfce.pid.module;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.isfce.pid.dao.IModuleJpaDao;
import org.isfce.pid.dao.IProfesseurJpaDao;
import org.isfce.pid.model.Cours;
import org.isfce.pid.model.Module;
import org.isfce.pid.model.Module.MAS;
import org.isfce.pid.model.Professeur;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles(value = "testU")
@Sql({ "/dataTestU.sql" })
@SpringBootTest
public class TestDaoModule {
	@Autowired
	IModuleJpaDao daoModule;
	@Autowired
	IProfesseurJpaDao daoProfesseur;
	Cours geb = new Cours("IGEB", "Gestion de base de données", (short) 60);

	// private Professeur ovo = new Professeur(1, "vo", "Didier", "vo@isfce.be",
	// User.);
	@Test
	@Transactional
	void testFindListModulesDeCours() {
		System.out.println("hhhhhhhggggggggggggg" + geb.getSections());
		System.out.println(geb.getNbPeriodes());
		geb.getSections().add("Informatique");
		List<Module> liste = daoModule.findByCoursOrderByMoment(geb);
		assertEquals(2, liste.size());
		System.out.println(liste.get(0).getCours());
		assertEquals(geb, liste.get(0).getCours());
		System.out.println(liste.get(0).getMoment().toString());
		assertEquals("APM", liste.get(0).getMoment().toString());
		System.out.println(liste.get(1).getMoment().toString());
		assertEquals("SOIR", liste.get(1).getMoment().toString());

		System.out.println(daoModule.generateCodeModule("IGEB"));

	}

	@Test
	@Transactional
	void testGetModuleSections() {
		List<Module> liste = daoModule.getModulesSection("Informatique", MAS.SOIR);
		liste.forEach(System.out::println);
		// lafdil an other way of doing it
		liste.forEach(x -> System.out.println(x));

		assertEquals(5, liste.size());

		List<Module> liste2 = daoModule.getModulesSection2("Informatique", MAS.APM);
		liste2.forEach(System.out::println);
		// assertEquals(3, liste2.size());

	}

//	@Test
//	@Transactional
//	void testGetModuleProfesseur() {
//		
//		
//	}

	@Test
	@Transactional
	void testGetModuleProfesseur() {
		Optional<Professeur> prof1 = daoProfesseur.findById(1);
		assertTrue(prof1.isPresent());
		List<Module> l1 = daoModule.getModuleProfesseur(prof1.get().getUser().getUsername());
		assertEquals(6, l1.size());
		l1.forEach(System.out::println);
		List<Module> l2 = daoModule.findModuleByProfesseurOrderByCode(prof1.get());
		assertEquals(6, l2.size());
		l2.forEach(System.out::println);

	}
	/*
	 * @Test String testgenerateCodeModule(geb) { System.out.println(geb); }
	 */
// 6Dec video1--26min
	@Test
	@Transactional
	void listetriee() {
		List<Module> liste = daoModule.findAll(Sort.by("code", "moment"));
		liste.forEach(System.out::println);
		assertEquals(8, liste.size());
	}

}